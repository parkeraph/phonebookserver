const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors")

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(express.static('build'));

phonebook =  [
    {
      "name": "Arto Hellas",
      "number": "040-123456",
      "id": 1
    },
    {
      "name": "Ada Lovelace",
      "number": "39-44-5323523",
      "id": 2
    },
    {
      "name": "Dan Abramov",
      "number": "12-43-234345",
      "id": 3
    },
    {
      "name": "Mary Poppendieck",
      "number": "39-23-6423122",
      "id": 4
    },
    {
      "name": "parker",
      "number": "12343",
      "id": 5
    }
]

const generateID = () => {
  do{
    newID = Math.floor(Math.random() * 100000);
  }while(phonebook.find(person => person.id === newID) !== undefined);

  return newID;
} 

const personExists = (name, number) => {
  if(phonebook.find(person => person.name === name) || phonebook.find(person => person.number === number)){
    return true;
  }else{
    return false;
  }
}

app.get('/api/persons', (req, res) => {
  console.log(" GET request at /api/persons"); 
  res.json(phonebook);
})

app.get('/api/persons/:id', (req, res) => {
  const reqID = Number(req.params.id);

  let person = phonebook.find(person => person.id === reqID)

  if(person === undefined){
    res.status(404).json({
      error: "record with id "+reqID+" not found"
    });
    return;
  }

  res.json(person);
})

app.delete('/api/persons/:id', (req, res) => {
  const reqID = Number(req.params.id);
  
  //check if person exists
  if(phonebook.find(person => person.id === reqID) === undefined){
    res.status(400).json({
      error: "record with ID "+reqID+ " does not exist"
    })
    return;
  }

  phonebook = phonebook.filter(person => person.id !== reqID);

  res.status(200).end();
})

app.post("/api/persons", (req, res) => {
  const body = req.body;

  if(!(body.name && body.number)){
    res.status(400).json({
      "error": "invalid post parameters"
    })
    return;
  } 

  if(personExists(body.name, body.number)){
    res.status(400).json({
      "error": "person or number already exists in database"
    })
    return;
  }

  const newPerson = {
    "name": body.name,
    "number": body.number,
    "id": generateID()
  }

  phonebook = phonebook.concat(newPerson);
  
  console.log("new entry added: \n", newPerson)

  res.status(200).json(newPerson);
}) 

app.get('/info', (req, res) => {
  let payload = "<p>There are "+phonebook.length+" people in the phonebook</p>";
  payload = payload + "<p>"+new Date()+"</p>";

  res.send(payload)
})

const PORT = process.env.PORT || 3001
app.listen(PORT, () => {
  console.log('Server is running on port',PORT);
})

